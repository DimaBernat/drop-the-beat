﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public Transform[] playersMission;
    int borderHeight = 4;
    static Level instance;

    private void Awake() {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
       
        
        CreateArena();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateArena() {
        GameObject arenaBorders = GameObject.CreatePrimitive(PrimitiveType.Cube);
        arenaBorders.transform.parent = transform;
        arenaBorders.GetComponent<MeshRenderer>().enabled = false;
        arenaBorders.transform.localPosition = new Vector3(transform.localScale.x, borderHeight, 0);
        arenaBorders.transform.localScale = new Vector3(0, borderHeight*2, transform.localScale.x*2);
        GameObject arenaBorders2 = Instantiate(arenaBorders, transform); //Object 2
        arenaBorders2.transform.localPosition = new Vector3((transform.localScale.x* -1), 4, 0);

        arenaBorders = Instantiate(arenaBorders, transform);//Object 3
        arenaBorders.transform.localScale = new Vector3(transform.localScale.x * 2, borderHeight *2, 0);
        arenaBorders.transform.localPosition = new Vector3(0, 4, (transform.localScale.x * -1));
        arenaBorders2 = Instantiate(arenaBorders, transform);//Object 4
        arenaBorders2.transform.localPosition = new Vector3(0, 4, transform.localScale.x);
    }

    public static Vector3 GetMissionV3(int player) {
        return instance.playersMission[player].position;
    }
}
