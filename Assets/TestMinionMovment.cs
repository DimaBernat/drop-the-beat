﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMinionMovment : MonoBehaviour
{
    Rigidbody m_rigi;
    Material m_mat;

    public Vector3 m_TestV3,missionV3;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        m_rigi = GetComponent<Rigidbody>();
        m_mat = GetComponent<MeshRenderer>().material;
        missionV3 = GameController.GetMissionVec(0);
    }

    // Update is called once per frame
    void Update()
    {
        m_TestV3 = missionV3 - transform.position  ;
        if (Input.GetKeyDown(KeyCode.A)) {
            print("Move?");
            m_rigi.AddForce(m_TestV3 * speed);
        }
        if (Input.GetKeyDown(KeyCode.S)) {
            print("Move?");
            m_rigi.AddForce(Vector3.forward * speed);
        }
    }
}
