﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioVisualizer : MonoBehaviour
{
    public float minHeight, maxHeight, updateSensitivity = 0.5f;

    public Color visualizerColor  =  Color.red;
    [Space(15)]
    public AudioClip audioClip;
    public bool loop = true;
    [Space(15) ,Range(64,8192)]
    public int visualizerSimples = 64;

    Image[] visualizerObjects;
    AudioSource m_audioSource;
    // Start is called before the first frame update
    void Start()
    {
        visualizerObjects = GetComponentsInChildren<Image>();

        if (audioClip) {
            m_audioSource = new GameObject("AudioSource").AddComponent<AudioSource>();
            m_audioSource.loop = loop;
            m_audioSource.clip = audioClip;
            m_audioSource.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {        
        float[] spectrumData = m_audioSource.GetSpectrumData(visualizerSimples, 0, FFTWindow.Rectangular);
        for (int i = 0; i < visualizerObjects.Length; i++) {
            RectTransform rect = visualizerObjects[i].GetComponent<RectTransform>();
            Vector2 newSize = rect.rect.size;
            newSize.y = Mathf.Lerp(newSize.y, minHeight + (spectrumData[i] * (maxHeight - minHeight) * 5f), updateSensitivity);
             rect.sizeDelta = newSize;
            visualizerObjects[i].color = visualizerColor;
        }
    }
}
