﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody),typeof(MeshRenderer))]
public class Minnion : MonoBehaviour
{
    public float[] playerInfluance;

    public float m_BasePower = 3,m_speed,m_InfluenceResistance = 0.1f, highestInfluance;
    public int minThreshold = 38, maxThreshold = 100, m_InfluenceThreshold;
    public enum MinionSubmission  { noan,player1,player2}
    public MinionSubmission m_CurrentRally,m_MissionZone;

    GameObject winnerUI;
    Rigidbody m_rigi;
    Material m_mat;
    // Start is called before the first frame update

    #region  ------ Init ------------
    void Start()
    {
        m_rigi = GetComponent<Rigidbody>();
        m_rigi.mass = Random.Range(8, 200);
        m_mat = GetComponent<MeshRenderer>().material;
        if (!M_Utilities.isDebug)
        {
            m_InfluenceThreshold = Random.Range(5, 50);
            m_InfluenceResistance = Random.Range(0.1f, 1.8f);
            print("<color=red>Minnion</color> inf:" + m_InfluenceThreshold +" Resistance: " + m_InfluenceResistance);
        }
        playerInfluance = new float[GameController.GetPlayersLenght];
    }


    #endregion------ Init ------------

    private void OnTriggerEnter(Collider other) {
        //print("Triggered");
     

            if (other.tag == "Mission") {
            m_rigi.drag = .9f;
            m_rigi.angularDrag = 0.9f;
            if (other.transform.name.Contains("Player1")) {
                m_MissionZone = MinionSubmission.player1;
             
                print("<color=red>Player1 Trigger Mission</color> " + gameObject.name);
            }
            if (other.transform.name.Contains("Player2")) {
                m_MissionZone = MinionSubmission.player2;
                print("<color=red>Player2 Trigger Mission</color> " + gameObject.name);
            }
            if ((int)m_MissionZone == 0)
                return;
            GameController.Mission((int)m_MissionZone,false);
            m_BasePower = 0;


        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Mission" && (int)m_MissionZone != 0)
        {
            m_rigi.drag = 0f;
            m_rigi.angularDrag = 0f;
            GameController.Mission((int)m_MissionZone, true);
            if (other.transform.name.Contains("Player1"))
            {
                m_MissionZone = MinionSubmission.noan;
                //print("<color=red>Player1 Left Trigger Mission</color> " + gameObject.name);
            }
            if (other.transform.name.Contains("Player2"))
            {
                m_MissionZone = MinionSubmission.noan;
                //print("<color=red>Player2 Left Trigger Mission</color> " + gameObject.name);
            }             
        }
    }

    private void OnCollisionEnter(Collision collision) {
        //print("Colided");
    }
    void Act(float strenght,int player) {
        Jump(strenght / 200);
        ColorChange(strenght);
        MoveToMision(strenght, player);
    }

    void ColorChange(float speed)
    {
        //m_mat.color = Color.Lerp(Color.white, Color.clear, 5);
        StartCoroutine("ColorFlash");
    }

    void Jump(float power) 
    {
        //power *= GameController.GetMinionPowerMultiplayer;
        m_rigi.velocity = new Vector3(0, (m_BasePower + power),0);
    }

    void MoveToMision(float power,int player) {
        Vector3 targetDiracstion = GameController.GetMissionVec(player) - transform.position;
        power *= GameController.GetMinionPowerMultiplayer;
        //print("Moveing to Objective " + power);
        m_rigi.AddForce(targetDiracstion * power);
    }

    void ChangeMinnionSub(int player)
    {
        if (m_CurrentRally != (MinionSubmission)player + 1)
        {
        Debug.Log("<color=red>ChangeMinnionSub Mission CRally:</color> " + (int)m_CurrentRally);
            GameController.ChangeMinnionSub((int)m_CurrentRally, false); //Subtract from the old Raly
            m_CurrentRally = (MinionSubmission)player + 1;
            GameController.ChangeMinnionSub((int)m_CurrentRally, true);
            Debug.Log("<color=red>ChangeMinnionSub Mission After CRally:</color> " + (int)m_CurrentRally);
        }
    }

    public void AddInfluance(float strenght,int player)
    {
        if (m_CurrentRally != MinionSubmission.noan)
        {
            float resistance;
            if ((int)m_CurrentRally == player)
            {
                resistance = 0.7f;
            }
            else
            {
                resistance = 1.5f;
            }
                Debug.Log("<color=red>AddInfluance: </color> " + (int)m_CurrentRally + " p: " + player);
        playerInfluance[player] += (strenght / (m_InfluenceResistance*resistance));
        }
        else
        {
            playerInfluance[player] += (strenght / m_InfluenceResistance );
        }
        
        if (playerInfluance[player] > highestInfluance)
        {
            highestInfluance = playerInfluance[player];
            if (playerInfluance[player] > m_InfluenceThreshold)
            {
                Debug.Log("<color=red>Calculate Influance: </color> " + playerInfluance[player].ToString() + " Strenght " + strenght + " InfluResis " + m_InfluenceResistance);
                ChangeMinnionSub(player);
                if (strenght != 0)
                {
                Act(strenght + highestInfluance, player);
                }
            }
        }

    }

    public int GetMinionRally()
    {
        return (int)m_CurrentRally;
    }

    IEnumerator ColorFlash()
    {
        float t = 0;
        while (t < .5f)
        {
            t += Time.deltaTime;
            m_mat.color = Color32.Lerp(Color.white, Color.green, Mathf.PingPong(Time.time, 1f));
            yield  return null;
           
        }
        m_mat.color = Color.white;
    }
}
