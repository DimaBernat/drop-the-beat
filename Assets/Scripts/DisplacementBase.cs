﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DisplacementBase : MonoBehaviour
{
    public MeshRenderer mesh;
    public float displacementAmount;
    public ParticleSystem explosionParticles;

    public int spectrumNote;
    public float minHeight, maxHeight, acstionThreshold = 0.5f;

    //public Color visualizerColor = Color.red;
    [Space(15)]
    public AudioClip audioClip;
    public bool loop = true;
    [Space(15), Range(64, 8192)]
    public int visualizerSimples = 64;


    AudioSource m_audioSource;
    // Start is called before the first frame update
    void Start() {
        if (audioClip) {
            if (GameObject.Find("AudioSource")) {
                m_audioSource = GameObject.Find("AudioSource").AddComponent<AudioSource>();
                m_audioSource.loop = loop;
                m_audioSource.clip = audioClip;
                m_audioSource.Play();
                return;
            }
            m_audioSource = new GameObject("AudioSource").AddComponent<AudioSource>();
            m_audioSource.loop = loop;
            m_audioSource.clip = audioClip;
            m_audioSource.Play();
        }
    }
    float[] spectrumData = new float [64];
    // Update is called once per frame
    void Update()
    {     
        m_audioSource.GetSpectrumData(spectrumData, 0, FFTWindow.Rectangular);
            Action(spectrumData[spectrumNote]);   
    }

    public virtual void Action(float spectrumBeat) {

    }
}
