﻿
using UnityEngine;
public static class M_Utilities 
{
    public static KeyCode[] m_Player1Key, m_Player2Key; //TODO: Set in opstions
    public static bool isDebug = false;

    public static void InitPlayersKey(int player, KeyCode[] keycodes)
    {
        if (player ==0)
        {
            m_Player1Key = keycodes;
        }
        else
        {
            m_Player2Key = keycodes;
        }
    }

    public static AudioClip LoadClip(string path)
    {
        WWW audioLoader = new WWW(path);
        while (!audioLoader.isDone)
            System.Threading.Thread.Sleep(100);

        AudioClip sound = audioLoader.GetAudioClip(true, false);

        int counter = 0;
        while (sound == null)
        {
            //do not load longer than 5seconds
            if (counter++ > 10)
                break;
            else
                System.Threading.Thread.Sleep(100);
        }

        sound = audioLoader.GetAudioClip();
        return sound;
    }
}
