﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class BasePlayer : MonoBehaviour
{
    public AudioSource m_BeatAudioSource,m_AudioAnim;
    
    public float timeCounter;
    protected bool  isAI;
    public int playerID, m_Note;

    public TMPro.TextMeshProUGUI playerNameTxt, m_BeatStreakTxt,m_minnionsRallyTxt;
    public UnityEngine.UI.Image minnionFill;
    protected AudioClip[] m_BeatClips;
    protected float m_LastBeat = 0  ,m_CurrentBeatTime, m_OffBeatMargin = 0.4f, m_MinionMultiplyer;

    protected int m_BeatStreak, m_MaxNotes = 4;
    protected bool isOn;
    protected const float m_DanceBeat = 1f, m_Margin = 0.4f, m_DanceSongL = 3;
    protected MinnionActsions m_CurrentAcstion;

    
    public BeatStatus m_BeatStatus;

    protected void InitAudio()
    {
   
        //float me = 0.1f;
        //Debug.Log("<color=red>InitAudio</color>" + (float)120/ (float)100);
        m_BeatAudioSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        m_BeatAudioSource.volume = 0.7f;
        //m_BeatAudioSource.clip = GameController.GetBeatClip(0);
        m_BeatClips = GameController.GetBeatClip;
        m_AudioAnim = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        m_AudioAnim.clip = GameController.GetAnimClip[0];
        var audioMix = Resources.Load("Player") as AudioMixer;
        if (audioMix != null)
        {
            var groupMix = audioMix.FindMatchingGroups("Master");
            int audioMixLenght = groupMix.Length;
            foreach (var item in audioMix.FindMatchingGroups("Master"))
            {
                print("<color=blue> Mixer: </color>" + audioMix.FindMatchingGroups("Master")[1].name);
            }
            //print("<color=blue> Mixer: </color>"+ d.outputAudioMixerGroup.audioMixer);
            //var audioMixGroup = audioMix.FindMatchingGroups("Master")[0];
            m_BeatAudioSource.outputAudioMixerGroup = groupMix[audioMixLenght-1];
            m_AudioAnim.outputAudioMixerGroup = groupMix[audioMixLenght-2];
        }
        SetMinnionRally(0);
    }

    public bool RecoredBeat(float beatTimeDifrance)
    {
        m_CurrentBeatTime = Time.timeSinceLevelLoad;
        print(playerID + "<color=blue> RecoredBeat </color>" + (m_CurrentBeatTime - m_LastBeat));
        if ((m_CurrentBeatTime - m_LastBeat) <= (beatTimeDifrance + m_OffBeatMargin) && (m_CurrentBeatTime - m_LastBeat) >= (beatTimeDifrance - m_OffBeatMargin))
        {
            m_Note++;
            BeatNote();
                return true;
        }
        else
        {
            //m_OffBeatMargin = m_Margin;
            //m_BeatStreak = 0;
            //Bad Vibe
            return false;
        }
      
    }

   public void BeatStatusChange(BeatStatus status)
    {
        if (m_BeatStatus == status)
        {
            return;
        }
        m_BeatStatus = status;
        switch (m_BeatStatus)
        {
            case BeatStatus.idle:
                ChangeUITextColor(Color.white);
                break;
            case BeatStatus.inBeat:
                ChangeUITextColor(Color.magenta);
                break;
            case BeatStatus.inAnim:
                ChangeUITextColor(Color.blue);
                break;
            default:
                break;
        }
    }

   public void BeatNote()
    {
        m_BeatAudioSource.Play();
    }

    public void SetMinnionRally(int minnions) {
        m_minnionsRallyTxt.text = minnions.ToString() + "\\" + GameController.GetMaxMinions;
        minnionFill.fillAmount = (float)((float)minnions / (float)GameController.GetMaxMinions);
    }

    void ChangeUITextColor(Color color)
    {
        playerNameTxt.color = color;
    }


    

    IEnumerator SongAnim(float durastion)
    {
        yield return new WaitForSeconds(m_DanceSongL >= m_AudioAnim.clip.length ? m_AudioAnim.clip.length : m_DanceSongL);
        print(playerID + "<color=yellow> audioAnim Lenght: </color>" + (timeCounter - Time.realtimeSinceStartup) +" time:"+ m_AudioAnim.time + " tSample " + m_AudioAnim.timeSamples);
        ChangeUITextColor(Color.white);
        m_AudioAnim.Stop();
       
        print(playerID + "Stop");
        yield return new WaitForSeconds(durastion);
        BeatStatusChange(BeatStatus.idle);
    }

    IEnumerator FailedAnim(float durastion)
    {
        yield return new WaitForSecondsRealtime(durastion);
        m_AudioAnim.Stop();
        BeatStatusChange(BeatStatus.idle);
    }

    public bool IsOn { get { return isOn; } set { isOn = value; } }
}
