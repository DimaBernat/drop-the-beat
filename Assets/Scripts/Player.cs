﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : BasePlayer
{
    public TMPro.TextMeshProUGUI m_text;

    bool isPlayer1 = true,isComp ;

    int id;
    MinnionActsions currentBeat;
    KeyCode[] m_ActsionKey;
    BeatStatus m_CurrentStatus;
    AiPlayer m_Ai;
    void Start()
    {

        for (int i = 0; i < System.Enum.GetNames(typeof(MinnionActsions)).Length; i++)
        {
            if (isPlayer1)
            {
            m_ActsionKey[i] = M_Utilities.m_Player1Key[i];
            }
            else
            {
                m_ActsionKey[i] = M_Utilities.m_Player2Key[i];
            }
        }
        
        
        m_BeatAudioSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        m_BeatAudioSource.clip = GameController.GetBeatClip[0];
   
    }
    // Update is called once per frame
    void Update()
    {
    
        if (Input.GetKeyDown(KeyCode.A))
        {
            //m_AudioSource.Play();
            //print("2");
        }

        //if (Input.GetKeyDown(m_ActsionKey))
        //    {
        //        switch (m_CurrentStatus)
        //        {
        //            case BeatStatus.idle:
        //                BeatStatusChange(BeatStatus.inBeat);
        //                m_Note = 0;//TODO Reset
        //                m_Note++;
        //                m_LastBeat = Time.timeSinceLevelLoad;
        //                print("Note " + m_Note);
        //                BeatNote();
        //                break;
        //            case BeatStatus.inBeat:
        //            if (base.RecoredBeat(m_DanceBeat))
        //            {
        //                if (m_Note == m_MaxNotes)//Dance Monkey
        //                {
        //                    BeatStatusChange(BeatStatus.inAnim);
        //                    BeatSuccsesfull();
        //                    print("<color=red>BeatSuccsesfull</color> " + m_BeatStreak);
        //                    return;
        //                }
        //            }
        //            else
        //            {
        //                m_OffBeatMargin = m_Margin;
        //                m_BeatStreak = 0;
        //                //        //Bad Vibe
        //            }
        //            m_LastBeat = m_CurrentBeatTime;
        //            //RecoredBeat();
        //            break;
        //            case BeatStatus.inAnim:
        //                break;
        //        }

        //    }
    }

    
    void BeatSuccsesfull()
    {

        HandleBeatStreak();
        print("Beat " + m_BeatStreak);
        StartCoroutine("SongAnim", m_DanceSongL);
        m_OffBeatMargin *= 0.99f;
        GameController.AddInfluance(m_BeatStreak, id);
    }

    void BeatNote()
    {
        m_BeatAudioSource.Play();
    }

    void BeatStatusChange(BeatStatus stat)
    {
        if (m_CurrentStatus == stat)
        {
            return;
        }
        m_CurrentStatus = stat;
        switch (m_CurrentStatus)
        {
            case BeatStatus.idle:
                m_text.color = Color.white;
                break;
            case BeatStatus.inBeat:
                m_text.color = Color.magenta;
                break;
            case BeatStatus.inAnim:
                m_text.color = Color.blue;
                break;
            default:
                break;
        }
    }

    void HandleBeatStreak()
    {
        m_BeatStreak++;
        m_BeatStreakTxt.text = m_BeatStreak.ToString();
    }

public  BeatStatus GetBeatStatus { get { return m_CurrentStatus; } }

    IEnumerator SongAnim( float durastion)
    {
        yield return new WaitForSecondsRealtime(durastion);
        BeatStatusChange(BeatStatus.idle);
    }
}

