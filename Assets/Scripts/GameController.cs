﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BeatStatus { idle, inBeat, inAnim }
public enum MinnionActsions { dance,move,berserk}
public class GameController : MonoBehaviour
{
    public Minnion[] m_Minions;
    public GameObject m_BallPref, playerUIGO;
    public Transform m_GameBoared;
    public BasePlayer[] m_Players;
    public AudioClip[] m_BeatClips, m_AnimSingin;


    public int m_MaxMinions, m_CurrentPlayerCount;
    public float m_Delay, minionPowerMultiplayer = 50;

    Vector3 player1Mission, player2Mission;
    UnityEngine.UI.RawImage winnerGO;
    public TMPro.TextMeshProUGUI winnerTxt;
    int[] minionInMission,minionRally;


    //int m_MaxLittlePpl;
    static GameController instance;
    #region   ----------- Mono -----------

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start() {
        m_Minions = new Minnion[m_MaxMinions];
        StartCoroutine(CreateMinions());
        //CreateMinions();

        player1Mission = Level.GetMissionV3(0);
        player2Mission = Level.GetMissionV3(1);

        InitAIPlayers(2);

        //GameObject.FindObjectOfType<Canvas>()
        GameObject go = new GameObject("winner").AddComponent<RectTransform>().gameObject;
        //GameObject go = Instantiate(winnerGO, winnerGO.transform);
        go.transform.parent = GameObject.FindObjectOfType<Canvas>().transform;
        winnerGO = go.AddComponent<UnityEngine.UI.RawImage>();
        winnerGO.color = Color.clear;
        RectTransform rect = go.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(Screen.width, Screen.height);
        rect.localPosition = Vector3.zero;

        go = new GameObject("WinnerTxt");
        go.AddComponent<TMPro.TextMeshProUGUI>();
        go.transform.parent = rect; //Transform.FindObjectOfType<Canvas>().transform;
        rect = go.GetComponent<RectTransform>();
        rect.localPosition = Vector3.zero;
        winnerTxt = go.GetComponent<TMPro.TextMeshProUGUI>();
        winnerTxt.alignment = TMPro.TextAlignmentOptions.Midline;
        winnerTxt.enableWordWrapping = false;
        winnerTxt.text = "";
        winnerTxt.font = (TMPro.TMP_FontAsset)Resources.Load("edo SDF");
        winnerTxt.color = Color.red;

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            foreach (var item in m_Players)
            {
                item.gameObject.active = !item.gameObject.active;
            }
        }
    }

    #endregion  ----------- Mono -----------

    void InitAIPlayers(int howMany)
    {
        m_Players = new BasePlayer[(howMany)];
        minionInMission = new int[(howMany)];
        minionRally = new int[(howMany+1)];
        for (int i = 0; i < howMany; i++)
        {
            GameObject go = new GameObject("AIPlayers");
            AiPlayer ai = go.AddComponent<AiPlayer>();
            ai.playerID = i;
            m_Players[i] = ai;
            ai.InitGui(GameObject.FindObjectOfType<Canvas>().transform);
            //Debug.Log("AI CReated "+i);
        }

    }

    void InitAudioClipList()
    {

    }


    void InitPlayers() {
        //GameObject go = new GameObject("Players");
        //for (int i = 0; i < m_Players.Length; i++) {

        //    GameObject go2 = new GameObject("AI");
        //    go2.transform.parent = go.transform;
        //     var ko = go2.AddComponent<AiPlayer> ();
        //    m_Players[i].GetComponent<AiPlayer>().aiID = i + 50;
        //    m_Players[i] = ko;
        //    BasePlayer miniPOLay = m_Players[i];
        //}
    }


    void ActivatePlayers()
    {
        foreach (var player in m_Players)
        {
            player.IsOn = true;           
        }
    }

    void PlayerWon(int player)
    {
        winnerTxt.text = "Player " + player + " Won";
        foreach (var playerScript in m_Players)
        {
            try
            {
                var p = playerScript.GetComponent<AiPlayer>().IsAiOn = false;

            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }

    public static void AddInfluance(float beatStreak, int player)
    {
        foreach (var minion in instance.m_Minions)
        {
            minion.AddInfluance(beatStreak, player);
        }
    }

    #region  ----------- Public methods -----------

    public static float GetMinionPowerMultiplayer { get { return instance.minionPowerMultiplayer; } }


    public static Vector3 GetMissionVec(int i) {
        if (i == 0) {
            return instance.player1Mission;
        }
        if (i == 1) {
            return instance.player2Mission;
        }
        else {
            return Vector3.zero;
        }
    }

    public static void Mission(int player, bool isLeft)
    {
        player--;
        print(" score " + instance.minionInMission[player]);
        if (isLeft)
        {
            instance.minionInMission[player]--;
            return;
        }
        instance.minionInMission[player]++;
        if (instance.minionInMission[player] >= (instance.m_MaxMinions * 0.7)) //Handle Winner
        {
            Debug.LogError(player + " player Won " + instance.minionInMission[player]);
            foreach (var basePlayer in instance.m_Players)
            {
                basePlayer.IsOn = false;
            }
            //instance.winnerGO.color = Color.gray;
            instance.winnerGO.color = new Color(1, 1, 1, 0.7f);
            instance.PlayerWon(player);
        }
    }

    /// <summary>
    /// Counts all minions affilatsion and put it in "minionRally" 0 = noan  , 1 = P1 ,2 = p2
    /// </summary>
    public static bool GetRallyCount(int playerID)
    {
        instance.minionRally = new int[(instance.m_Players.Length + 1)]; //Make New list
        int returendAffilastion=0;
        foreach (var minion in instance.m_Minions)
        {
            returendAffilastion = minion.GetMinionRally(); //Returns  { noan,player1,player2} { 0,1,2}
            instance.minionRally[returendAffilastion]++; //Adds
      
        }
        for (int i = 0; i < instance.minionRally.Length; i++) //Go throught int list to see if playerID is the highest
        {
            if (instance.minionRally[i] >= (instance.m_MaxMinions/2) && i == playerID+1)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Returns true if this players  rallys half or more of the minnions
    /// </summary>
    public static bool CheckAmIWinning(int playerID)
    {

        if (instance.minionRally[playerID+1] >= (instance.m_MaxMinions / 2))
        {
            return true;
        }
        return false;
    }

    public static void ChangeMinnionSub(int affilastion,bool isPositive)
    {
     
        if (isPositive)
        {
        instance.minionRally[affilastion]++;

        }
        else
        {
            instance.minionRally[affilastion]--;
        }
        instance.SetMinnionRally();
    }

    void SetMinnionRally()
    {
        for (int i = 0; i < m_Players.Length; i++)
        {
            m_Players[i].SetMinnionRally(minionRally[i+1]);
        }
     
    
    }
    //public static AudioClip GetBeatClip(int song)
    //{
    //    if (song <= instance.m_BeatClips.Length)
    //    {
    //    return instance.m_BeatClips[song];

    //    }
    //    else
    //    {
    //        return instance.m_BeatClips[0];
    //    }
    //}
    #endregion ----------- Public methods -----------

    IEnumerator CreateMinions() {
        GameObject go = new GameObject("Minion Holder");
        for (int i = 0; i < m_MaxMinions; i++) {//Make a minions some were around the plane
            var v3 = new Vector3(Random.Range((-1) * (m_GameBoared.lossyScale.x * 5), m_GameBoared.lossyScale.x * 5), 5, Random.Range((-1) * (m_GameBoared.lossyScale.z * 5), m_GameBoared.lossyScale.z * 5));
            m_Minions[i] = Instantiate(m_BallPref, v3, Quaternion.identity).GetComponent<Minnion>();
            m_Minions[i].transform.name = "Minion" +i;
            m_Minions[i].transform.parent = go.transform;
            yield return new WaitForSeconds(m_Delay);
        }
        yield return null;
        ActivatePlayers();
    }
    #region  ---------- Getters & Setters --------------
    public static AudioClip[] GetBeatClip { get { return instance.m_BeatClips; } }
    public static AudioClip[] GetAnimClip { get { return instance.m_AnimSingin; } }
    public static GameObject GetPlayerUIGO { get { return instance.playerUIGO; } }
    public static int GetPlayersLenght { get { return instance.m_Players.Length; } }
    public static int GetMaxMinions { get { return instance.m_MaxMinions; } }
    #endregion ---------- Getters & Setters -------------
}
