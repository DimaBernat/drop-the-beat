﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        M_Utilities.m_Player1Key = new KeyCode[] { KeyCode.Q, KeyCode.W, KeyCode.E };
        M_Utilities.m_Player2Key = new KeyCode[] { KeyCode.B, KeyCode.N, KeyCode.M };
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
