﻿using System.Collections;
using UnityEngine;

public class AiPlayer : BasePlayer
{

    int m_SuccessRateNeed = 100, m_AIClickSuccsesRate, aiClickThreshold, successRateNeedMax = 20, clicked, totalClickedFailed, totalClickedSussced;
    float nextClickTime, failedAnimDur, responceTime = 0.1f;

    #region  ------ Init ------------
    private void Awake()
    {
        print("Awake");
    }

    protected void Start()
    {

        base.InitAudio();
        Init();
        //Debug.LogError("ai" + aiID);
    }

    public void Init()
    {
        //m_SuccessRateNeed = 100;//Random.Range(0, 100);
        m_AIClickSuccsesRate = 95;//Random.Range(40, 80); //TODO: Change to AI getting   Sucseesrate :70 = 0.24%||75 = 0.25-32%|| 80 = 0.36-.40%|| 85 = 0.45 -.47% ||  90 = 0.87-.82% || 95 = 100%
        //isAiOn = true;
        isAI = true;
        Debug.Log(playerID + " AI INi ClickSuccess " + m_AIClickSuccsesRate + " m_SuccessRateNeed: " + m_SuccessRateNeed);
    }

    public void InitGui(Transform tran)
    {
        GameObject go = Instantiate(GameController.GetPlayerUIGO, tran);

        foreach (var txtUgui in go.GetComponentsInChildren<TMPro.TextMeshProUGUI>())
        {
            if (txtUgui.transform.name.Contains("P Name"))
            {
                playerNameTxt = txtUgui;
                if (playerID != 1)
                {
                    playerNameTxt.text = "AI: " + playerID.ToString(); //TODO: Change to Name Of PLayer
                }
                else
                {
                    char[] reverse = ("AI: " + playerID.ToString()).ToCharArray() ;
                    System.Array.Reverse(reverse);
                    playerNameTxt.text = new string(reverse);
                }
            }
            if (txtUgui.transform.name.Contains("BeatStreak"))
            {
                m_BeatStreakTxt = txtUgui;
            }
            if (txtUgui.transform.name.Contains("Minion"))
            {
                m_minnionsRallyTxt = txtUgui;
                minnionFill = m_minnionsRallyTxt.transform.parent.GetComponent<UnityEngine.UI.Image>();
            }


        }
        if (playerID == 1)
        {
            var rect = go.GetComponent<RectTransform>();
            Debug.Log("Pos " + rect.position + " LocalPos: " + rect.localPosition);
            //rect.localPosition = new Vector3( Screen.width, 0, 0);
            rect.anchorMin = Vector2.one;
            rect.anchorMax = Vector2.one;
            rect.pivot = Vector2.one;
            foreach (var rectChild in go.GetComponentsInChildren<RectTransform>())
            {
                rectChild.anchorMin = Vector2.one;
                rectChild.anchorMax = Vector2.one;
                rectChild.pivot = Vector2.one;
            }
            rect = m_BeatStreakTxt.transform.parent.GetComponentInParent<RectTransform>();//Change Images Side        
            float widght = rect.sizeDelta.x;

            rect = m_BeatStreakTxt.GetComponentInParent<RectTransform>();

            rect.localPosition = new Vector3(-widght, rect.localPosition.y, rect.localPosition.z);
            m_BeatStreakTxt.alignment = TMPro.TextAlignmentOptions.MidlineRight;

            m_minnionsRallyTxt.alignment = TMPro.TextAlignmentOptions.MidlineRight;
            playerNameTxt.alignment = TMPro.TextAlignmentOptions.Center;
            rect = m_minnionsRallyTxt.GetComponentInParent<RectTransform>();
            //minnionFill = rect.transform.parent.GetComponent<UnityEngine.UI.Image>();
            rect.localPosition = new Vector3(-rect.parent.GetComponent<RectTransform>().sizeDelta.x, rect.localPosition.y, rect.localPosition.z);
        }
    }

    #endregion ------ MonoBehaivor ------------

    private void FixedUpdate()
    {
        if (isOn && m_BeatStatus == BeatStatus.idle)
        {
            ChooseWhichStratgey();
            PreClick();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            float ssFailedRate = ((float)totalClickedFailed / (float)(totalClickedSussced + totalClickedFailed)); //Calculate and post all the stats
            float sssussesRate = ((float)totalClickedSussced / (float)(totalClickedSussced + totalClickedFailed));
            Debug.LogError(playerID + "Stop" + clicked + " totalSucsesed:" + totalClickedSussced + " totalFailed " + totalClickedFailed + " Time: " + Time.realtimeSinceStartup + "  N: " + m_AIClickSuccsesRate + " SSSuccsesRate: " + sssussesRate + "%");
            //Debug.LogError("Stop" + clicked + " totalSucsesed:" + totalClickedSussced + " totalFailed " + totalClickedFailed + " Time: " + Time.realtimeSinceStartup + " SSFRate: " + ssFailedRate + "%  N" + m_AIClickSuccsesRate + " SSSuccsesRate: " + sssussesRate + "%");
        }

    }




    public void PreClick()
    {
        clicked++;
        //Debug.Log("<color=red>PreClick</color>" + clicked);
        if (ClickSuccess())
        {
            HandleSuccsesHit();
        }
        else
        {
            ClickFailed();
        }
    }

    public void SetSuccsesRate(int succsesRate)
    {
        m_AIClickSuccsesRate = succsesRate;
    }

    void HandleSuccsesHit()
    {

        switch (m_BeatStatus)
        {
            case BeatStatus.idle:
                BeatStatusChange(BeatStatus.inBeat);
                m_Note = 0;//TODO Reset
                m_Note++;
                m_LastBeat = Time.timeSinceLevelLoad;
                //print("Note " + m_Note);
                BeatNote();
                break;
            case BeatStatus.inBeat:

                if (base.RecoredBeat(m_DanceBeat))
                {
                    if (m_Note == m_MaxNotes)//Dance Monkey
                    {
                        BeatStatusChange(BeatStatus.inAnim);
                        BeatSuccsesfull();

                        return;
                    }
                }
                else
                {
                    ClickFailed();
                    return;
                }

                m_LastBeat = m_CurrentBeatTime;
                break;
            case BeatStatus.inAnim:
                break;
        }
        Invoke("PreClick", m_DanceBeat); // Cycle of next Time Clicking
        //ClickSuccess();
    }

    bool ClickSuccess()
    {
        int num = Random.Range(0, 100);
        //Debug.Log("AI ClickSuccess: " + (num + m_AIClickSuccsesRate));
        if ((num + m_AIClickSuccsesRate) > m_SuccessRateNeed)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void ClickFailed()
    {
        totalClickedFailed++;
        print(playerID + "<color=yellow>AI Failed </color>" + totalClickedFailed);
        BeatStatusChange(BeatStatus.inAnim);
        StartCoroutine("FailedAnim", failedAnimDur + responceTime);
        m_OffBeatMargin = m_Margin;
        m_BeatStreak = 0;
        m_BeatStreakTxt.text = m_BeatStreak.ToString();
        //Invoke("PreClick", m_DanceBeat);
    }

    void BeatSuccsesfull()
    {
        m_AudioAnim.Play();
        HandleBeatStreak();
        print(playerID + "<color=red>BeatSuccsesfull</color> " + m_BeatStreak);
        timeCounter = Time.realtimeSinceStartup;
        StartCoroutine("SongAnim", responceTime); //Add Responce Time
        m_OffBeatMargin *= 0.99f;
        GameController.AddInfluance(m_BeatStreak * m_MinionMultiplyer, playerID); //Send To Minnions
    }

    void HandleBeatStreak()
    {
        m_BeatStreak++;
        totalClickedSussced++;
        print(playerID + "<color=yellow>AI Succseded </color>" + totalClickedSussced);
        m_BeatStreakTxt.text = m_BeatStreak.ToString();
    }

    void ChooseWhichStratgey()
    {

        if (GameController.CheckAmIWinning(playerID))
        {
            if (m_BeatStreak > 2)
            {
                m_BeatAudioSource.clip = m_BeatClips[1]; //Move Stratgy
                m_CurrentAcstion = MinnionActsions.move;
                m_MinionMultiplyer = 1;
            }
            else
            {
                m_BeatAudioSource.clip = m_BeatClips[0]; //Berserk Dance
                m_CurrentAcstion = MinnionActsions.berserk;
                m_MinionMultiplyer = 0;
            }

        }
        else
        {
            m_BeatAudioSource.clip = m_BeatClips[2]; //Dance Stratgy
            m_CurrentAcstion = MinnionActsions.dance;
            m_MinionMultiplyer = 2;
        }

        //m_BeatAudioSource.clip = GameController.GetBeatClip(songNum);
    }
    //public bool IsAiOn { get { return isAiOn; } set { isAiOn = value; } }

    IEnumerator AIThinkProcsess(float waitingFor)
    {
        Debug.Log("Start AIThinkProcsess");
        //isGameOn = true;
        for (; ; )
        {
            yield return new WaitForSeconds(waitingFor);
            HandleSuccsesHit();
            Debug.Log("AI DropTheBeat");
        }
        Debug.Log("End AIThinkProcsess");
    }

    #region   Setting & Getting
    public int AiClickThreshold { get { return aiClickThreshold; } set { aiClickThreshold = value; } }

    public bool IsAiOn { set { isOn = value; } }
    #endregion
}
