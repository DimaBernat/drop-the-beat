﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplacementRegulare : DisplacementBase {
    void Update() {
        displacementAmount = Mathf.Lerp(displacementAmount, 0, Time.deltaTime);
        mesh.material.SetFloat("_Amount", displacementAmount);
        if ( Input.GetKeyDown(KeyCode.Space) || Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) {
            displacementAmount += 1f;
            explosionParticles.Play();
        }

    }
}
