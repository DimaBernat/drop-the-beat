﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplacementMusic : DisplacementBase
{
    public bool isConstantPLaying;
    [Range(0.1f, 5f)]
    public float disstorsionmultiplier;

    public float lastDisplacement;

     public  override void Action(float spectrumBeat) {
        if (isConstantPLaying) {

            displacementAmount = Mathf.Lerp(displacementAmount, minHeight + (spectrumBeat * (maxHeight - minHeight)), Time.deltaTime);
            mesh.material.SetFloat("_Amount", displacementAmount* disstorsionmultiplier);
            if (spectrumBeat > acstionThreshold) {
             
                explosionParticles.Play();
            }

        }
        else {
            if (displacementAmount < 0.03 && lastDisplacement != 0) {
                print("yay");
                lastDisplacement = 0;
            }
            if (spectrumBeat  > lastDisplacement + 0.2) {
                lastDisplacement = displacementAmount;
                displacementAmount = spectrumBeat;
                
                    explosionParticles.Play();
                

            
            }
                displacementAmount = Mathf.Lerp(displacementAmount , 0, Time.deltaTime);
                mesh.material.SetFloat("_Amount", displacementAmount*3);
        }
        //mesh.material.SetFloat("_Amount", displacementAmount);
    }
}
