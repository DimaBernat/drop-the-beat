﻿using System.Collections;
using UnityEngine;


public class Beat : MonoBehaviour
{
    #region  --- parameters ---

  

    int m_BeatStreak,m_Note,m_MaxNotes = 4;
    float m_LastBeat = 0, m_CurrentBeatTime,m_OffBeatMargin = 0.4f;

    const float m_DanceBeat = 1f,m_Margin = 0.4f,m_DanceSongL = 3;

    static Beat instance;
    #endregion  #region  --- parameters ---

    #region  --- Unity ---
    void Awake()
    {
        instance = this;
    }


    #endregion




}
